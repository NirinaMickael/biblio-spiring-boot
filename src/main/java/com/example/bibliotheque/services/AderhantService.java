package com.example.bibliotheque.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bibliotheque.domain.Adherant;
import com.example.bibliotheque.repository.AdherantRepository;

@Service
public class AderhantService {
    

    @Autowired
    private AdherantRepository adherantRepository;

    public AderhantService(){}

    public  List<Adherant> getAllAdherant(){
        List<Adherant> adherants = this.adherantRepository.findAll();
        return adherants;
    }

    public Adherant  createAdherant(Adherant adherant){
        return this.adherantRepository.save(adherant);
    }
}
