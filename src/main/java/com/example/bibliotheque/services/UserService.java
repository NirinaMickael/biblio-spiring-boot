package com.example.bibliotheque.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bibliotheque.domain.UserEntity;
import com.example.bibliotheque.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    
    public List<UserEntity> getAllUsers(){
        return this.userRepository.findAll();
    }
}
