package com.example.bibliotheque.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.bibliotheque.domain.Adherant;
import com.example.bibliotheque.services.AderhantService;

@Controller
@RequestMapping("adherant")
public class AdherantController {
    String templatePath = "adherant\\";

    @Autowired
    private AderhantService aderhantService;
    public AdherantController(){};

    @GetMapping
    public String getAll(){
        List<Adherant> adherants = this.aderhantService.getAllAdherant();
        System.out.println(adherants);
        return this.templatePath+"adherant-list.html";
    }
    @PostMapping
    public String create(){
        Adherant newAdherant = new Adherant("Nirina","prof","IPV 2 ter");
        Adherant adherant = this.aderhantService.createAdherant(newAdherant);
        System.out.println(adherant);
        return this.templatePath+"adherant-create.html";
    }
}
