package com.example.bibliotheque.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import  com.example.bibliotheque.domain.UserEntity;
import com.example.bibliotheque.services.UserService;;

@Controller
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping
    public String getAllUsers(){
        List<UserEntity> users = this.userService.getAllUsers();
        System.out.println(users);
        return  "index.html";
    }
}
