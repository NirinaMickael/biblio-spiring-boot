package com.example.bibliotheque.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.GenerationType;;
@Entity
public class Adherant {
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long Id;

    private String nom;
    private String role;
    private String adresse ;

    public Adherant(){}

    public Adherant(String nom,String role,String adresse){
        this.nom=nom;
        this.adresse= adresse;
        this.role=role;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getAdresse() {
        return adresse;
    }
    public String getNom() {
        return nom;
    }
    public String getRole() {
        return role;
    }

    public String toString(){
        return "Nom : "+this.nom+"\nRole : "+this.role+"\ndresse : "+this.adresse; 
    }
}
