package com.example.bibliotheque.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "UserEntity")
public class UserEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String nom;

    public UserEntity(String nom){
        this.nom = nom;
    }
    public UserEntity(){}
    

    public String toString(){
        return  "Nom : "+this.nom+"\n";    
    }
}