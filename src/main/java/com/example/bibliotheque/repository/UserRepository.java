package com.example.bibliotheque.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bibliotheque.domain.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity,Long>{
    
}
