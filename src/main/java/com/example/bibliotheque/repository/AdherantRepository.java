package com.example.bibliotheque.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.bibliotheque.domain.Adherant;

public interface AdherantRepository extends JpaRepository<Adherant,Long> {
    
}
